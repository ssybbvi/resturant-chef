let Koa2 = require('koa')
let KoaStatic = require('koa-static2')
let path = require('path')
let fs = require('fs')

const app = new Koa2()
const env = process.env.NODE_ENV || 'development' // Current mode

app
    .use(KoaStatic('/', path.resolve(__dirname, '../build')))
    .use((ctx, next) => {
        console.log("ctx.url", ctx.url)
        if (ctx.url.indexOf(".") === -1) {
            let html = fs.readFileSync(path.join(__dirname, '../build/index.html')).toString()
            return ctx.body = html;
        } else {
            next()
        }
    })

app.listen("8527")