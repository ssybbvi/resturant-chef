import io from 'socket.io-client';

import {
    System
} from '../config'

let url = System.webSocketUrl


let socketInstance = io(url, {
    transports: ['websocket', 'xhr-polling', 'jsonp-polling']
})
export let subscriptionSocket = (eventName, cb) => {
    socketInstance.on(eventName, cb);
    return () => {
        socketInstance.removeListener(eventName, cb);
    }
}