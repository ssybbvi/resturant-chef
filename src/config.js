const DevBaseUrl = "http://localhost:3000"
const ProdBashUrl = "http://35.234.17.43:3000"

export let System = {
    baseURL: process.env.NODE_ENV !== "production" ? DevBaseUrl : ProdBashUrl, // 配置API接口地址
    webSocketUrl: process.env.NODE_ENV !== "production" ? DevBaseUrl : ProdBashUrl
}